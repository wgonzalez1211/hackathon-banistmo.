package com.banitsmo.lectura;

import au.com.bytecode.opencsv.CSVReader;
import com.banistmo.constante.Constantes;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class Lecturas {

    static Logger logger = Logger.getLogger(Lecturas.class);
    private FileReader fileReader;
    private CSVReader reader;

    public ArrayList<String[]> leerCvs() {
        ArrayList<String[]> datos = new ArrayList<>();
        try {
            fileReader = new FileReader(Constantes.getCache().getRutaArchivo());
            reader = new CSVReader(fileReader, Constantes.getCache().getSeparador());
            String[] nextLine;

            while ((nextLine = reader.readNext()) != null) {
                datos.add(nextLine);
            }

        } catch (IOException e) {
            logger.error("Se presentaron problemas para leer el archivo cvs, descripcion del problema", e);
        }
        cerrarConexiones();
        return datos;
    }

    private void cerrarConexiones() {
        try {
            if (reader != null) {
                reader.close();
            }
            if (fileReader != null) {
                fileReader.close();
            }
        } catch (IOException e) {
            logger.error("Se presentaron problemas para cerrar los recursos del cvs, descripcion del problema", e);
        }
    }
}
