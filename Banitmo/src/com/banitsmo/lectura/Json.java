package com.banitsmo.lectura;

import com.banitsmo.dto.Cliente;
import com.banitsmo.utilidades.Conversiones;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class Json {

    static Logger logger = Logger.getLogger(Json.class);

    public List<Cliente> convertirDatos(ArrayList<String[]> mapeo) {
        Conversiones conversiones = new Conversiones();
        List<Cliente> datos = new ArrayList<>();
        try {

            for (int i = 0; i < mapeo.size(); i++) {

                if (i == 0) {
                    logger.info("Este dto no se tendra encuentra debido a que corresponde al encabezado");
                } else {
                    Cliente cliente = new Cliente();
                    cliente.setCiudad(mapeo.get(i)[0]);
                    cliente.setCorregimiento(mapeo.get(i)[1]);
                    cliente.setDiasMorisidad(mapeo.get(i)[2]);
                    cliente.setEdad(conversiones.convertirEntero(mapeo.get(i)[3]));
                    cliente.setEstabilidadLaboral(mapeo.get(i)[4]);
                    cliente.setIdentificacion(mapeo.get(i)[5]);
                    cliente.setLugarTrabajo(mapeo.get(i)[6]);
                    cliente.setOtrosIngresos(mapeo.get(i)[7]);
                    cliente.setProvincia(mapeo.get(i)[8]);
                    cliente.setSexo(mapeo.get(i)[9]);
                    cliente.setSalario(conversiones.convertirDouble(mapeo.get(i)[10]));
                    cliente.setCore(mapeo.get(i)[11]);
                    cliente.setCoreOriginal(mapeo.get(i)[12]);
                    datos.add(cliente);
                }

            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas para convertir los datos del cvs a json ", e);
        }
        return datos;
    }

    public void imprimirJson(List<Cliente> datos) {
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        String json = gson.toJson(datos);
        logger.info("El json generado fue ");
        logger.info(json);
    }
}
