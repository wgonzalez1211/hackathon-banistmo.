package com.banitsmo.riesgo;

import com.banistmo.constante.Constantes;
import com.banitsmo.dto.Cliente;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class Riesgo {

    static Logger logger = Logger.getLogger(Riesgo.class);
    private List<Cliente> cliente;
    private List<String[]> modeloRiesgo;
    private String[] mpoDatos;
    private int contador;

    public Riesgo() {
        this.modeloRiesgo = new ArrayList();
        this.contador = 1;
    }

    public void medirRiesgoEdad() {

        for (Cliente client : cliente) {
            try {
                if (client.getEdad() >= Constantes.getCache().getEdadMinima() && client.getEdad() < Constantes.getCache().getEdadMaxima()) {
                    medirRiesgoSalario(client);
                } else {
                    mapearDatos(client, "La edad no cumple el modelo de riesgo");
                }
                contador++;
            } catch (Exception e) {
                logger.error("Se presentaron incosistencias con el modelo cliente ", e);
            }
        }

    }

    public void medirRiesgoSalario(Cliente cliente) {
        if (cliente.getSalario() > Constantes.getCache().getSalarioMinimo() && cliente.getSalario() < Constantes.getCache().getSalarioMaximo()) {
            medirListaClinton(cliente);
        } else {
            mapearDatos(cliente, "El salario presenta un riesgo financiero");
        }

    }

    public void medirListaClinton(Cliente cliente) {
        if (!Constantes.getCache().getListaClinton().contains(cliente.getIdentificacion())) {
            mapearDatos(cliente, "Este cliente se encuentra aprobado");
        } else {
            mapearDatos(cliente, "La cedula se encuentra registrado en la lista clinton");
        }

    }

    public void medirListaClintonCiudad(Cliente cliente) {
        if (!Constantes.getCache().getCiudadesClinton().contains(cliente.getLugarTrabajo())) {

        } else {
            mapearDatos(cliente, "La ciudad se encuentra registrado en la lista clinton");
        }

    }

    private void mapearDatos(Cliente cliente, String descripcion) {
        mpoDatos = new String[4];
        mpoDatos[0] = String.valueOf(contador);
        mpoDatos[1] = String.valueOf(cliente.getEdad());
        mpoDatos[2] = String.valueOf(cliente.getSalario());
        mpoDatos[3] = descripcion;
        modeloRiesgo.add(mpoDatos);

    }

    //Methos get and set
    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }

    public List<String[]> getModeloRiesgo() {
        return modeloRiesgo;
    }

    public void setModeloRiesgo(List<String[]> modeloRiesgo) {
        this.modeloRiesgo = modeloRiesgo;
    }

    public String[] getMpoDatos() {
        return mpoDatos;
    }

    public void setMpoDatos(String[] mpoDatos) {
        this.mpoDatos = mpoDatos;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }
}
