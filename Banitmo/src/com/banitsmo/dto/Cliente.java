package com.banitsmo.dto;

public class Cliente {

    private String ciudad;
    private String corregimiento;
    private String diasMorisidad;
    private int edad;
    private String estabilidadLaboral;
    private String identificacion;
    private String lugarTrabajo;
    private String otrosIngresos;
    private String provincia;
    private String sexo;
    private double salario;
    private String core;
    private String coreOriginal;
    private String nombreCliente;

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCorregimiento() {
        return corregimiento;
    }

    public void setCorregimiento(String corregimiento) {
        this.corregimiento = corregimiento;
    }

    public String getDiasMorisidad() {
        return diasMorisidad;
    }

    public void setDiasMorisidad(String diasMorisidad) {
        this.diasMorisidad = diasMorisidad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEstabilidadLaboral() {
        return estabilidadLaboral;
    }

    public void setEstabilidadLaboral(String estabilidadLaboral) {
        this.estabilidadLaboral = estabilidadLaboral;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getLugarTrabajo() {
        return lugarTrabajo;
    }

    public void setLugarTrabajo(String lugarTrabajo) {
        this.lugarTrabajo = lugarTrabajo;
    }

    public String getOtrosIngresos() {
        return otrosIngresos;
    }

    public void setOtrosIngresos(String otrosIngresos) {
        this.otrosIngresos = otrosIngresos;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getCore() {
        return core;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public String getCoreOriginal() {
        return coreOriginal;
    }

    public void setCoreOriginal(String coreOriginal) {
        this.coreOriginal = coreOriginal;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

}
