package com.banitsmo.utilidades;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import org.apache.log4j.Logger;

public class Archivo {

    static Logger logger = Logger.getLogger(Archivo.class);

    public void copiarArchivo(String rutaOrigen, String rutaDestino) {
        try {
            Path origenPath = FileSystems.getDefault().getPath(rutaOrigen);
            Path destinoPath = FileSystems.getDefault().getPath(rutaDestino);
            Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error("Se presentaron problemas para hacer un analisis externo", e);
        }
    }
}
