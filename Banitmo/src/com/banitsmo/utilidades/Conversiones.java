package com.banitsmo.utilidades;

import org.apache.log4j.Logger;

public class Conversiones {

    static Logger logger = Logger.getLogger(Conversiones.class);

    public int convertirEntero(String dato) {
        try {
            return Integer.parseInt(dato);
        } catch (NumberFormatException e) {
            logger.error("El dato ingresado no es alfa numerico ", e);
            return 0;
        }
    }

    public double convertirDouble(String dato) {
        try {
            return Double.parseDouble(dato);
        } catch (NumberFormatException e) {
            logger.error("El dato ingresado no es alfa numerico ", e);
            return 0;
        }
    }

}
