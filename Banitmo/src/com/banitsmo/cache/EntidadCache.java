package com.banitsmo.cache;

import java.util.List;

public class EntidadCache {

    private static EntidadCache entidad_Cache;
    private char separador;
    private String rutaArchivo;
    private int edadMinima;
    private int edadMaxima;
    private double salarioMinimo;
    private double salarioMaximo;
    private List<String> listaClinton;
    private List<String> ciudadesClinton;
    private String rutaAnalisisInterno;
    private String nombreArchivoInterno;

   

    private EntidadCache() {

    }

    public synchronized static EntidadCache getEntidad_Cache() {

        if (entidad_Cache == null) {
            entidad_Cache = new EntidadCache();
        }
        return entidad_Cache;
    }

    public static boolean isNull() {
        boolean flag;
        flag = entidad_Cache == null;
        return flag;
    }

    // Metodos get and set
    public char getSeparador() {
        return separador;
    }

    public void setSeparador(char separador) {
        this.separador = separador;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public int getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(int edadMinima) {
        this.edadMinima = edadMinima;
    }

    public int getEdadMaxima() {
        return edadMaxima;
    }

    public void setEdadMaxima(int edadMaxima) {
        this.edadMaxima = edadMaxima;
    }

    public double getSalarioMinimo() {
        return salarioMinimo;
    }

    public void setSalarioMinimo(double salarioMinimo) {
        this.salarioMinimo = salarioMinimo;
    }

    public double getSalarioMaximo() {
        return salarioMaximo;
    }

    public void setSalarioMaximo(double salarioMaximo) {
        this.salarioMaximo = salarioMaximo;
    }

    public List<String> getListaClinton() {
        return listaClinton;
    }

    public void setListaClinton(List<String> listaClinton) {
        this.listaClinton = listaClinton;
    }

    public List<String> getCiudadesClinton() {
        return ciudadesClinton;
    }

    public void setCiudadesClinton(List<String> ciudadesClinton) {
        this.ciudadesClinton = ciudadesClinton;
    }

    public String getRutaAnalisisInterno() {
        return rutaAnalisisInterno;
    }
     public String getNombreArchivoInterno() {
        return nombreArchivoInterno;
    }

    public void setNombreArchivoInterno(String nombreArchivoInterno) {
        this.nombreArchivoInterno = nombreArchivoInterno;
    }

    public void setRutaAnalisisInterno(String rutaAnalisisInterno) {
        this.rutaAnalisisInterno = rutaAnalisisInterno;
    }

}
