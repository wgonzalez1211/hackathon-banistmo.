package com.banitsmo.cache;

import com.banistmo.constante.Constantes;
import com.banitsmo.principal.Formulario;
import com.banitsmo.utilidades.Conversiones;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Propertie implements Serializable {

    private static final long serialVersionUID = -378434221142782176L;
    static Logger logger = Logger.getLogger(Propertie.class);
    private EntidadCache cache;

    public void leerProperties() throws IOException {
        Conversiones conversiones = new Conversiones();
        Properties prop = new Properties();
        InputStream input = null;
        try {
            logger.debug("Comenzo el metodo getProperties de la clase  Propertie");

            cache = EntidadCache.getEntidad_Cache();
            input = new FileInputStream("Resources/Properties/Banitsmo.properties");
            prop.load(input);
            cache.setSeparador(prop.getProperty("Banitsmo.cvs.lectura.separador").charAt(0));
            cache.setRutaArchivo(prop.getProperty("Banitsmo.cvs.lectura.archivo"));
            cache.setEdadMinima(conversiones.convertirEntero(Formulario.jTextField1.getText()));
            cache.setEdadMaxima(conversiones.convertirEntero(Formulario.jTextField2.getText()));
            cache.setSalarioMinimo(conversiones.convertirDouble(Formulario.jTextField3.getText()));
            cache.setSalarioMaximo(conversiones.convertirDouble(Formulario.jTextField4.getText()));
            cache.setRutaAnalisisInterno(prop.getProperty("Banitsmo.cvs.ruta.archivo.interno"));
            cache.setNombreArchivoInterno(prop.getProperty("Banitsmo.cvs.nombre.archivo.interno"));
            llenarListaClinton();
            logger.debug("Termino el metodo getProperties de la clase  Propertie");
        } catch (IOException ex) {
            logger.error("Error inexperado al intentar Leer el archivo Properties 0x2541A  " + ex.getMessage(), ex);

        } finally {
            if (input != null) {
                input.close();
            }
        }

    }

    private void llenarListaClinton() {
        List<String> listaClinton = new ArrayList<>();
        for (int k = 0; k < Formulario.jComboBox1.getItemCount(); k++) {
            listaClinton.add(Formulario.jComboBox1.getItemAt(k));
        }
        cache.setListaClinton(listaClinton);
        llenarListaClintonPaises();
    }

    private void llenarListaClintonPaises() {
        List<String> ciudadesClinton = new ArrayList<>();
        for (int k = 0; k < Formulario.jComboBox2.getItemCount(); k++) {
            ciudadesClinton.add(Formulario.jComboBox2.getItemAt(k));
        }
        cache.setCiudadesClinton(ciudadesClinton);
        Constantes.setCache(cache);
    }
}
