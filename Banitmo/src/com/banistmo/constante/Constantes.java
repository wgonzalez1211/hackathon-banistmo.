package com.banistmo.constante;

import com.banitsmo.cache.EntidadCache;

public class Constantes {

    private static EntidadCache cache;

    public static EntidadCache getCache() {
        return cache;
    }

    public static void setCache(EntidadCache cache) {
        Constantes.cache = cache;
    }
}
