package com.banistmo.escritura;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Escritura {

    static Logger logger = Logger.getLogger(Escritura.class);
    private String rutaArchivo;
    private List<String[]> datos;
    private String[] encabezado;

    public void duplicarArchivo(String ruta, String nombre) {

        try {
            StringBuilder nombreArchivo = new StringBuilder();
            Date fechRegistro = new Date();
            nombreArchivo.append(ruta);
            nombreArchivo.append(".xls");
            Path origenPath = FileSystems.getDefault().getPath(nombreArchivo.toString());
            nombreArchivo = new StringBuilder();
            nombreArchivo.append(nombre);
            nombreArchivo.append("_");
            nombreArchivo.append(fechRegistro.getTime());
            nombreArchivo.append(".xls");
            rutaArchivo = nombreArchivo.toString();
            Path destinoPath = FileSystems.getDefault().getPath(rutaArchivo);
            Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
            escribirInforme();
        } catch (IOException e) {
            logger.error("Se presentaron problemas para duplicar el informe ", e);
        }
    }

    public void escribirInforme() {
        try {

            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("Resumen");
            int fila = 0;
            HSSFRow row = sheet.createRow(fila);
            for (int r = 0; r < encabezado.length; r++) {
                HSSFCell cell = row.createCell(r);
                cell.setCellValue(encabezado[r]);

            }

            fila = 1;
            for (int r = 0; r < datos.size(); r++) {
                row = sheet.createRow(fila);
                for (int c = 0; c < datos.get(r).length; c++) {
                    HSSFCell cell = row.createCell(c);
                    cell.setCellValue(datos.get(r)[c]);
                }
                fila++;
            }

            try (FileOutputStream fileOut = new FileOutputStream(rutaArchivo, false)) {
                wb.write(fileOut);
                fileOut.flush();
            }
        } catch (IOException e) {
            logger.error("Se presentaron problemas para escribir el informe ", e);
        }

    }

    //Methodos get and set
    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public List<String[]> getDatos() {
        return datos;
    }

    public void setDatos(List<String[]> datos) {
        this.datos = datos;
    }

    public String[] getEncabezado() {
        return encabezado;
    }

    public void setEncabezado(String[] encabezado) {
        this.encabezado = encabezado;
    }

}
